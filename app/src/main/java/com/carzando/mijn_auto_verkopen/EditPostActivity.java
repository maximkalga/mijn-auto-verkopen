package com.carzando.mijn_auto_verkopen;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.support.v4.app.DialogFragment;
import android.app.DatePickerDialog;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.carzando.mijn_auto_verkopen.models.User;
import com.carzando.mijn_auto_verkopen.models.Post;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import android.view.View.OnClickListener;
import android.app.AlertDialog;

public class EditPostActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private int selImagePosition = -1;
    private int MAX_IMAGE_SIZE = 1024;
    private Button btnSelect;
    private ImageView ivImage;
    private String userChoosenTask;

    private static final String TAG = "EditPostActivity";
    private static final String REQUIRED = "Required";
    public static final String EXTRA_POST_KEY = "post_key";

    private DatabaseReference mDatabase;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    private EditText mTitleField;
    private EditText mBodyField;
    private EditText mPriceField;
    private TextView mDateField;
    private EditText mMileageField;
    private EditText mKwField;
    private EditText mCylinderField;
    private TextView mNumberOfCylindersField;
    private AutoCompleteTextView mMakeField;
    private AutoCompleteTextView mModelField;

    // uicontrols
    NoDefaultSpinner spTransmission;
    NoDefaultSpinner spFuel;
    NoDefaultSpinner spColor;
    NoDefaultSpinner spDoors;
    NoDefaultSpinner spSeats;

    // local members
    String stransmission;
    String sfuel;
    String scolor;
    String sdoors;
    String sseats;

    final String LOG_TAG = "myLogs";
    private String pictureImagePath = "";
    private ArrayList<String> photos = new ArrayList<>();
    private ArrayList<String> photosToDelete = new ArrayList<>();
    private ListView lvMain;
    private ImageAdapter imageAdapter;
    private DatabaseReference mPostReference;
    private DatabaseReference mUserPostReference;
    private ValueEventListener mPostListener;
    private String mPostKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        ivImage = (ImageView) findViewById(R.id.ivImage);
        ivImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage();
            }
        });

        findViewById(R.id.fab_submit_post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPost();
            }
        });

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // [START initialize_database_ref]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END initialize_database_ref]

        // Get post key from intent
        mPostKey = getIntent().getStringExtra(EXTRA_POST_KEY);
        if (mPostKey == null) {
            throw new IllegalArgumentException("Must pass EXTRA_POST_KEY");
        }
        // Initialize Database
        final String userId = getUid();
        mPostReference = FirebaseDatabase.getInstance().getReference()
                .child("posts").child(mPostKey);
        mUserPostReference = FirebaseDatabase.getInstance().getReference()
                .child("user-posts").child(userId).child(mPostKey);

        mTitleField = (EditText) findViewById(R.id.field_title);
        mBodyField = (EditText) findViewById(R.id.field_body);
        mPriceField = (EditText) findViewById(R.id.field_price);
        mDateField = (TextView) findViewById(R.id.showDate);
        spTransmission = (NoDefaultSpinner) findViewById(R.id.field_transmission);
        spFuel = (NoDefaultSpinner) findViewById(R.id.field_fuel);
        spColor = (NoDefaultSpinner) findViewById(R.id.field_color);
        spDoors = (NoDefaultSpinner) findViewById(R.id.field_doors);
        spSeats = (NoDefaultSpinner) findViewById(R.id.field_seats);
        mMileageField = (EditText) findViewById(R.id.field_mileage);
        mKwField = (EditText) findViewById(R.id.field_kw);
        mCylinderField = (EditText) findViewById(R.id.field_cylinder);
        mNumberOfCylindersField = (EditText) findViewById(R.id.field_number_of_cylinders);

        spTransmission.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                stransmission = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spFuel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                sfuel = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                scolor = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spDoors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                sdoors = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spSeats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                sseats = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        // Получаем ссылку на элемент AutoCompleteTextView в разметке
        AutoCompleteTextView makeTextView = (AutoCompleteTextView) findViewById(R.id.field_make);
        AutoCompleteTextView modelTextView = (AutoCompleteTextView) findViewById(R.id.field_model);
        // Получаем массив строк для автозаполнения
        String[] makes = getResources().getStringArray(R.array.makes);
        String[] models = getResources().getStringArray(R.array.models);
        // Создаем адаптер для автозаполнения элемента AutoCompleteTextView
        ArrayAdapter<String> modelAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, makes);
        ArrayAdapter<String> makeAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, models);
        makeTextView.setAdapter(modelAdapter);
        modelTextView.setAdapter(makeAdapter);

        lvMain = (ListView) findViewById(R.id.lvMain);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(LOG_TAG, "itemClick: position = " + position + ", id = " + id);
                selImagePosition = position;
                selectImage();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        // Add value event listener to the post
        // [START post_value_event_listener]
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                try {
                    Post post = dataSnapshot.getValue(Post.class);

                    mBodyField = (EditText) findViewById(R.id.field_body);
                    mPriceField = (EditText) findViewById(R.id.field_price);
                    mDateField = (TextView) findViewById(R.id.showDate);
                    spTransmission = (NoDefaultSpinner) findViewById(R.id.field_transmission);
                    spFuel = (NoDefaultSpinner) findViewById(R.id.field_fuel);
                    spColor = (NoDefaultSpinner) findViewById(R.id.field_color);
                    spDoors = (NoDefaultSpinner) findViewById(R.id.field_doors);
                    spSeats = (NoDefaultSpinner) findViewById(R.id.field_seats);
                    mMileageField = (EditText) findViewById(R.id.field_mileage);
                    mKwField = (EditText) findViewById(R.id.field_kw);
                    mCylinderField = (EditText) findViewById(R.id.field_cylinder);
                    mNumberOfCylindersField = (EditText) findViewById(R.id.field_number_of_cylinders);
                    mMakeField = (AutoCompleteTextView) findViewById(R.id.field_make);
                    mModelField = (AutoCompleteTextView) findViewById(R.id.field_model);

                    // [START_EXCLUDE]
                    mTitleField.setText(post.title);
                    mMakeField.setText(post.make);
                    mModelField.setText(post.model);
                    mBodyField.setText(post.body);
                    mPriceField.setText(post.price);
                    mDateField.setText(post.date);

                    if (post.mileage.length() > 0) {
                        mMileageField.setText(post.mileage);
                    }
                    if (post.kw.length() > 0) {
                        mKwField.setText(post.kw);
                    }
                    if (post.cylinder.length() > 0) {
                        mCylinderField.setText(post.cylinder);
                    }
                    if (post.number_of_cylinders.length() > 0) {
                        mNumberOfCylindersField.setText(post.number_of_cylinders);
                    }
                    if (post.transmission.length() > 0) {
                        spTransmission.setSelection(getIndex(spTransmission, post.transmission));
                    }
                    if (post.fuel.length() > 0) {
                        spFuel.setSelection(getIndex(spFuel, post.fuel));
                    }
                    if (post.color.length() > 0) {
                        spColor.setSelection(getIndex(spColor, post.color));
                    }
                    if (post.doors.length() > 0) {
                        spDoors.setSelection(getIndex(spDoors, post.doors));
                    }
                    if (post.seats.length() > 0) {
                        spSeats.setSelection(getIndex(spSeats, post.seats));
                    }

                    lvMain = (ListView) findViewById(R.id.lvMain);
                    if (post.photo_paths.length() > 0) {
                        String[] photo_paths = post.photo_paths.split(", ");
                        if (photos.isEmpty()) {
                            for (String s : photo_paths) {
                                photos.add(s);
                            }
                            imageAdapter = new ImageAdapter(getBaseContext(), photo_paths);
                            imageAdapter.notifyDataSetChanged();
                            lvMain.setAdapter(imageAdapter);
                            ListViewUtility.setListViewHeightBasedOnChildren(lvMain);
                        }
                    }

                } catch (NullPointerException e) {};
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // [START_EXCLUDE]
                Toast.makeText(EditPostActivity.this, "Failed to load post.",
                        Toast.LENGTH_SHORT).show();
                // [END_EXCLUDE]
            }
        };
        mUserPostReference.addValueEventListener(postListener);
        // [END post_value_event_listener]

        // Keep copy of post listener so we can remove it when app stops
        mPostListener = postListener;
    }

    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    private void submitPost() {
        final String title = mTitleField.getText().toString();
        final String body = mBodyField.getText().toString();
        final String price = mPriceField.getText().toString();
        final String date = mDateField.getText().toString();
        final String mileage = mMileageField.getText().toString();
        final String kw = mKwField.getText().toString();
        final String cylinder = mCylinderField.getText().toString();
        final String number_of_cylinders = mNumberOfCylindersField.getText().toString();
        final String make = mMakeField.getText().toString();
        final String model = mModelField.getText().toString();
        String pre_transmission = String.valueOf(spTransmission.getSelectedItem());
        if (pre_transmission == "null") {
            pre_transmission = "";
        }
        final String transmission = pre_transmission;
        String pre_fuel = String.valueOf(spFuel.getSelectedItem());
        if (pre_fuel == "null") {
            pre_fuel = "";
        }
        final String fuel = pre_fuel;
        String pre_color = String.valueOf(spColor.getSelectedItem());
        if (pre_color == "null") {
            pre_color = "";
        }
        final String color = pre_color;
        String pre_doors = String.valueOf(spDoors.getSelectedItem());
        if (pre_doors == "null") {
            pre_doors = "";
        }
        final String doors = pre_doors;
        String pre_seats = String.valueOf(spSeats.getSelectedItem());
        if (pre_seats == "null") {
            pre_seats = "";
        }
        final String seats = pre_seats;

        // Title is required
        if (TextUtils.isEmpty(title)) {
            mTitleField.setError(REQUIRED);
            return;
        }
        else {
            mTitleField.setError(null);
        }

        //Make is required
        if (TextUtils.isEmpty(make)) {
            mMakeField.setError(REQUIRED);
            return;
        }
        else {
            mMakeField.setError(null);
        }

        //Model is required
        if (TextUtils.isEmpty(model)) {
            mModelField.setError(REQUIRED);
            return;
        }
        else {
            mModelField.setError(null);
        }

        //Price is required
        if (TextUtils.isEmpty(price)) {
            mPriceField.setError(REQUIRED);
            return;
        }
        else {
            mPriceField.setError(null);
        }

        // Body is required
        if (TextUtils.isEmpty(body)) {
            mBodyField.setError(REQUIRED);
            return;
        }
        else {
            mBodyField.setError(null);
        }

        //Date is required
        if (TextUtils.isEmpty(date)) {
            mDateField.setError(REQUIRED);
            return;
        }
        else {
            mDateField.setError(null);
        }

        // [START single_value_read]
        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);

                        // [START_EXCLUDE]
                        if (userId.isEmpty()) {
                            // User is null, error out
                            Log.e(TAG, "User " + userId + " is unexpectedly null");
                            Toast.makeText(EditPostActivity.this,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new post
                            writeNewPost(userId, title, body, price, date,
                                    transmission, fuel, color, doors, seats, mileage, kw,
                                    cylinder, number_of_cylinders, make, model);
                        }

                        // Finish this Activity, back to the stream
                        finish();
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                    }
                });
        // [END single_value_read]

    }

    // [START write_fan_out]
    private void writeNewPost(String userId, String title, String body,
                              String price, String date, String transmission, String fuel,
                              String color, String doors, String seats, String mileage,
                              String kw, String cylinder, String number_of_cylinders,
                              String make, String model) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously

        String photo_paths = "";
        for (String s : photos)
        {
            String[] parts = s.split("/");
            photo_paths += "/images/" + mPostKey + "/" + parts[parts.length-1] + ", ";
        }

        Post post = new Post(userId, title, body, price, date, transmission,
                fuel, color, doors, seats, mileage, kw, cylinder, number_of_cylinders,
                make, model, photo_paths);
        Map<String, Object> postValues = post.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + mPostKey, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + mPostKey, postValues);

        StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
        if (!photos.isEmpty()) {
            for (final String p : photos) {
                final Uri photoUri = Uri.parse(p);
                StorageReference riversRef = storageRef.child("images/" + mPostKey + "/" + photoUri.getLastPathSegment());
                UploadTask uploadTask = riversRef.putFile(photoUri);

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();

                        // TODO: delete temp file from gallery
                        //File file = new File(p);
                        //boolean deleted = file.delete();
                        //System.out.println(deleted);

                    }
                });
            }
        }

        // Delete old(changed) photos
        for (String p : photosToDelete) {
            if(!photos.contains(p)) {
                StorageReference desertRef = storageRef.child(p);
                desertRef.delete();
            }
        }
        mDatabase.updateChildren(childUpdates);
    }
    // [END write_fan_out]

    public void datePicker(View view){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "date");
    }

    private void setDate(final Calendar calendar) {
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        ((TextView) findViewById(R.id.showDate)).setText(dateFormat.format(calendar.getTime()));

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar cal = new GregorianCalendar(year, month, day);
        setDate(cal);
    }

    public static class DatePickerFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener)
                            getActivity(), year, month, day);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void addImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(EditPostActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(EditPostActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectImage() {
        final CharSequence[] items = { "Remove current photo", "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(EditPostActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(EditPostActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();

                } else if (items[item].equals("Remove current photo")) {
                    userChoosenTask = "Remove current photo";

                    String oldPhoto = photos.get(selImagePosition);
                    if(photos.contains(oldPhoto)) photosToDelete.add(oldPhoto);
                    photos.remove(selImagePosition);
                    String[] stockArr = new String[photos.size()];
                    stockArr = photos.toArray(stockArr);
                    imageAdapter = new ImageAdapter(getBaseContext(), stockArr);
                    lvMain.setAdapter(imageAdapter);

                    ListViewUtility.setListViewHeightBasedOnChildren(lvMain);

                    selImagePosition = -1;
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        String imageFileName = System.currentTimeMillis() + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
        File file = new File(pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);}
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }

        String[] stockArr = new String[photos.size()];
        stockArr = photos.toArray(stockArr);
        imageAdapter = new ImageAdapter(this, stockArr);
        lvMain.setAdapter(imageAdapter);

        ListViewUtility.setListViewHeightBasedOnChildren(lvMain);
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    private void onCaptureImageResult(Intent data) {

        File imgFile = new File(pictureImagePath);
        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            Bitmap scaledBitmap = scaleDown(myBitmap, MAX_IMAGE_SIZE, true);
            Uri tempUri = getImageUri(getApplicationContext(), scaledBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            try {
                java.net.URI juri = new java.net.URI(finalFile.toURI().toString());
                if (selImagePosition == -1) {
                    photos.add(juri.toString());
                }
                else {
                    String oldPhoto = photos.get(selImagePosition);
                    if(photos.contains(oldPhoto)) photosToDelete.add(oldPhoto);
                    photos.remove(selImagePosition);
                    photos.add(selImagePosition, juri.toString());
                    selImagePosition = -1;
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(
                inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    //@SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImage = data.getData();
        String url;
        try {
            Bitmap scaledBitmap;
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inSampleSize = 1;
            Bitmap myBitmap = BitmapFactory.decodeStream(getBaseContext().getContentResolver().openInputStream(selectedImage), null, o);
            if (myBitmap.getWidth() > MAX_IMAGE_SIZE || myBitmap.getHeight() > MAX_IMAGE_SIZE) {
                scaledBitmap = scaleDown(myBitmap, MAX_IMAGE_SIZE, true);
                Uri tempUri = getImageUri(getApplicationContext(), scaledBitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                url = finalFile.toURI().toString();
                try {
                    java.net.URI juri = new java.net.URI(url);
                    if (selImagePosition == -1) {
                        photos.add(juri.toString());
                    } else {
                        String oldPhoto = photos.get(selImagePosition);
                        if(photos.contains(oldPhoto)) photosToDelete.add(oldPhoto);
                        photos.remove(selImagePosition);
                        photos.add(selImagePosition, juri.toString());
                        selImagePosition = -1;
                    }
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                if (selImagePosition == -1) {
                    photos.add(selectedImage.toString());
                }
                else {
                    String oldPhoto = photos.get(selImagePosition);
                    if(photos.contains(oldPhoto)) photosToDelete.add(oldPhoto);
                    photos.remove(selImagePosition);
                    photos.add(selImagePosition, selectedImage.toString());
                    selImagePosition = -1;
                }
            }
        } catch (FileNotFoundException e) {}
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Auto is aangepast wilt u aanpassingen bewaren?")
                .setCancelable(false)
                .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditPostActivity.this.finish();
                    }
                })
                .setNegativeButton("Geen", null)
                .show();
    }
}
