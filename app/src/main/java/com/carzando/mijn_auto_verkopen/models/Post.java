package com.carzando.mijn_auto_verkopen.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class Post {

    public String uid;
    //public String author;
    public String title;
    public String body;
    public String price;
    public String date;
    public String transmission;
    public String fuel;
    public String color;
    public String doors;
    public String seats;
    public String mileage;
    public String kw;
    public String cylinder;
    public String number_of_cylinders;
    public String make;
    public String model;
    public String photo_paths;

    public int starCount = 0;
    public Map<String, Boolean> stars = new HashMap<>();

    public Post() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Post(String uid,
                //String author,
                String title,
                String body,
                String price,
                String date,
                String transmission,
                String fuel,
                String color,
                String doors,
                String seats,
                String mileage,
                String kw,
                String cylinder,
                String number_of_cylinders,
                String make,
                String model,
                String photo_paths) {
        this.uid = uid;
        //this.author = author;
        this.title = title;
        this.body = body;
        this.price = price;
        this.date = date;
        this.transmission = transmission;
        this.fuel = fuel;
        this.color = color;
        this.doors = doors;
        this.seats = seats;
        this.mileage = mileage;
        this.kw = kw;
        this.cylinder = cylinder;
        this.number_of_cylinders = number_of_cylinders;
        this.make = make;
        this.model = model;
        this.photo_paths = photo_paths;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        //result.put("author", author);
        result.put("title", title);
        result.put("body", body);
        result.put("price", price);
        result.put("date", date);
        result.put("transmission", transmission);
        result.put("fuel", fuel);
        result.put("color", color);
        result.put("doors", doors);
        result.put("seats", seats);
        result.put("starCount", starCount);
        result.put("stars", stars);
        result.put("mileage", mileage);
        result.put("kw", kw);
        result.put("cylinder", cylinder);
        result.put("number_of_cylinders", number_of_cylinders);
        result.put("make", make);
        result.put("model", model);
        result.put("photo_paths", photo_paths);

        return result;
    }
    // [END post_to_map]

}
// [END post_class]
