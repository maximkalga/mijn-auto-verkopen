package com.carzando.mijn_auto_verkopen.fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

public class MyPostsFragment extends PostListFragment {

    public MyPostsFragment() {}

    @Override
    public Query getQuery(DatabaseReference databaseReference) {
        if (getUid().contains("YTdft7enADXWuAEmkGecMlh7Wjk1")) { //admin user id
            return databaseReference.child("posts");
            //TODO: переделать проверку по полю юзера isAdmin
        } else {
            return databaseReference.child("user-posts").child(getUid());
        }
    }
}
