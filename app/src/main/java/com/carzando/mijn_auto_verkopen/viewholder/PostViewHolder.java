package com.carzando.mijn_auto_verkopen.viewholder;

import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.carzando.mijn_auto_verkopen.DownloadImageTask;
import com.carzando.mijn_auto_verkopen.R;
import com.carzando.mijn_auto_verkopen.models.Post;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class PostViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public TextView authorView;
    public ImageView starView;
    public TextView numStarsView;
    public TextView bodyView;
    public TextView priceView;
    public ImageView imageView;

    private FirebaseStorage storage = FirebaseStorage.getInstance();

    public PostViewHolder(View itemView) {
        super(itemView);

        titleView = (TextView) itemView.findViewById(R.id.post_title);
        //authorView = (TextView) itemView.findViewById(R.id.post_author);
        //starView = (ImageView) itemView.findViewById(R.id.star);
        //numStarsView = (TextView) itemView.findViewById(R.id.post_num_stars);
        bodyView = (TextView) itemView.findViewById(R.id.post_body);
        priceView = (TextView) itemView.findViewById(R.id.post_price);
    }

    public void bindToPost(Post post, View.OnClickListener starClickListener) {
        titleView.setText(post.title);
        //authorView.setText(post.author);
        //numStarsView.setText(String.valueOf(post.starCount));
        String body = post.body;
        if (post.body.length() > 100) {
            body = post.body.substring(0, 100) + "...";
        }
        bodyView.setText(body);
        priceView.setText(post.price + " €");


        if (post.photo_paths.length() > 0) {
            final String mainPhoto = post.photo_paths.split(",")[0];
            StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
            storageRef.child(mainPhoto).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    new DownloadImageTask((ImageView) itemView.findViewById(R.id.icon)).execute(uri.toString());
                    // TODO: mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // TODO: добавить задержку и повторить загрузку
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
                            storageRef.child(mainPhoto).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    new DownloadImageTask((ImageView) itemView.findViewById(R.id.icon)).execute(uri.toString());
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // TODO: добавить задержку и повторить загрузку
                                }
                            });
                        }
                    }, 2000);
                }
            });
        }
        //starView.setOnClickListener(starClickListener);
    }
}
