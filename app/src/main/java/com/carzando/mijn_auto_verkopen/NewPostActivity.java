package com.carzando.mijn_auto_verkopen;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.support.v4.app.DialogFragment;
import android.app.DatePickerDialog;

import com.carzando.mijn_auto_verkopen.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.carzando.mijn_auto_verkopen.models.Post;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import android.view.View.OnClickListener;
import android.app.AlertDialog;

public class NewPostActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private int selImagePosition = -1;
    private int MAX_IMAGE_SIZE = 800; // 1024;
    private ImageView ivImage;
    private String userChoosenTask;

    private static final String TAG = "NewPostActivity";
    private static final String REQUIRED = "Required";

    private DatabaseReference mDatabase;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    private EditText mTitleField;
    private EditText mBodyField;
    private EditText mPriceField;
    private TextView mDateField;
    private EditText mMileageField;
    private EditText mKwField;
    private EditText mCylinderField;
    private TextView mNumberOfCylindersField;
    private AutoCompleteTextView mMakeField;
    private AutoCompleteTextView mModelField;

    // uicontrols
    NoDefaultSpinner spTransmission;
    NoDefaultSpinner spFuel;
    NoDefaultSpinner spColor;
    NoDefaultSpinner spDoors;
    NoDefaultSpinner spSeats;

    // local members
    String stransmission;
    String sfuel;
    String scolor;
    String sdoors;
    String sseats;

    final String LOG_TAG = "myLogs";
    private String pictureImagePath = "";
    private ArrayList<String> photos = new ArrayList<>();
    private ListView lvMain;
    private ImageAdapter imageAdapter;
    private String subject;
    private String email_body;
    private FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ivImage = (ImageView) findViewById(R.id.ivImage);
        ivImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage();
            }
        });

        findViewById(R.id.fab_submit_post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPost();
                findViewById(R.id.fab_submit_post).setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                        public void run() {
                            findViewById(R.id.fab_submit_post).setEnabled(true);
                        }
                    }, 1500
                );
            }
        });

        // [START initialize_database_ref]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END initialize_database_ref]

        mTitleField = (EditText) findViewById(R.id.field_title);
        mBodyField = (EditText) findViewById(R.id.field_body);
        mPriceField = (EditText) findViewById(R.id.field_price);
        mDateField = (TextView) findViewById(R.id.showDate);
        spTransmission = (NoDefaultSpinner) findViewById(R.id.field_transmission);
        spFuel = (NoDefaultSpinner) findViewById(R.id.field_fuel);
        spColor = (NoDefaultSpinner) findViewById(R.id.field_color);
        spDoors = (NoDefaultSpinner) findViewById(R.id.field_doors);
        spSeats = (NoDefaultSpinner) findViewById(R.id.field_seats);
        mMileageField = (EditText) findViewById(R.id.field_mileage);
        mKwField = (EditText) findViewById(R.id.field_kw);
        mCylinderField = (EditText) findViewById(R.id.field_cylinder);
        mNumberOfCylindersField = (EditText) findViewById(R.id.field_number_of_cylinders);
        mMakeField = (AutoCompleteTextView) findViewById(R.id.field_make);
        mModelField = (AutoCompleteTextView) findViewById(R.id.field_model);

        spTransmission.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                stransmission = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spFuel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                sfuel = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                scolor = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spDoors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                sdoors = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        spSeats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                sseats = String.valueOf(adapter.getItemAtPosition(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO
            }
        });

        // Получаем ссылку на элемент AutoCompleteTextView в разметке
        AutoCompleteTextView makeTextView = (AutoCompleteTextView) findViewById(R.id.field_make);
        AutoCompleteTextView modelTextView = (AutoCompleteTextView) findViewById(R.id.field_model);
        // Получаем массив строк для автозаполнения
        String[] makes = getResources().getStringArray(R.array.makes);
        String[] models = getResources().getStringArray(R.array.models);
        // Создаем адаптер для автозаполнения элемента AutoCompleteTextView
        ArrayAdapter<String> modelAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, makes);
        ArrayAdapter<String> makeAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, models);
        makeTextView.setAdapter(modelAdapter);
        modelTextView.setAdapter(makeAdapter);

        lvMain = (ListView) findViewById(R.id.lvMain);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, final View view,
                                    final int position, long id) {
                selImagePosition = position;
                selectImage();
            }
        });
    }

    private void submitPost() {
        final String title = mTitleField.getText().toString();
        final String body = mBodyField.getText().toString();
        final String price = mPriceField.getText().toString();
        final String date = mDateField.getText().toString();
        final String mileage = mMileageField.getText().toString();
        final String kw = mKwField.getText().toString();
        final String cylinder = mCylinderField.getText().toString();
        final String number_of_cylinders = mNumberOfCylindersField.getText().toString();
        final String make = mMakeField.getText().toString();
        final String model = mModelField.getText().toString();
        String pre_transmission = String.valueOf(spTransmission.getSelectedItem());
        if (pre_transmission == "null") {
            pre_transmission = "";
        }
        final String transmission = pre_transmission;
        String pre_fuel = String.valueOf(spFuel.getSelectedItem());
        if (pre_fuel == "null") {
            pre_fuel = "";
        }
        final String fuel = pre_fuel;
        String pre_color = String.valueOf(spColor.getSelectedItem());
        if (pre_color == "null") {
            pre_color = "";
        }
        final String color = pre_color;
        String pre_doors = String.valueOf(spDoors.getSelectedItem());
        if (pre_doors == "null") {
            pre_doors = "";
        }
        final String doors = pre_doors;
        String pre_seats = String.valueOf(spSeats.getSelectedItem());
        if (pre_seats == "null") {
            pre_seats = "";
        }
        final String seats = pre_seats;

        // Title is required
        if (TextUtils.isEmpty(title)) {
            mTitleField.setError(REQUIRED);
            mTitleField.requestFocus();
            return;
        }
        else {
            mTitleField.setError(null);
        }

        //Make is required
        if (TextUtils.isEmpty(make)) {
            mMakeField.setError(REQUIRED);
            mMakeField.requestFocus();
            return;
        }
        else {
            mMakeField.setError(null);
        }

        //Model is required
        if (TextUtils.isEmpty(model)) {
            mModelField.setError(REQUIRED);
            mModelField.requestFocus();
            return;
        }
        else {
            mModelField.setError(null);
        }

        //Price is required
        if (TextUtils.isEmpty(price)) {
            mPriceField.setError(REQUIRED);
            mPriceField.requestFocus();
            return;
        }
        else {
            mPriceField.setError(null);
        }

        // Body is required
        if (TextUtils.isEmpty(body)) {
            mBodyField.setError(REQUIRED);
            mBodyField.requestFocus();
            return;
        }
        else {
            mBodyField.setError(null);
        }

        //Date is required
        if (TextUtils.isEmpty(date)) {
            mDateField.setError(REQUIRED);
            mDateField.requestFocus();
            return;
        }
        else {
            mDateField.setError(null);
        }

        // [START single_value_read]
        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get user value
                        User user = dataSnapshot.getValue(User.class);

                        // [START_EXCLUDE]
                        if (userId.isEmpty()) {

                            // User is null, error out
                            Log.e(TAG, "User " + userId + " is unexpectedly null");
                            Toast.makeText(NewPostActivity.this,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new post
                            writeNewPost(userId, title, body, price, date,
                                    transmission, fuel, color, doors, seats, mileage, kw,
                                    cylinder, number_of_cylinders, make, model);
                        }
                        // Finish this Activity, back to the stream
                        finish();
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                    }
                });
        // [END single_value_read]

    }

    // [START write_fan_out]
    private void writeNewPost(String userId, String title, String body,
                              String price, String date, String transmission, String fuel,
                              String color, String doors, String seats, String mileage,
                              String kw, String cylinder, String number_of_cylinders,
                              String make, String model) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("posts").push().getKey();

        String photo_paths = "";
        for (String s : photos)
        {
            String[] parts = s.split("/");
            photo_paths += "/images/" + key + "/" + parts[parts.length-1] + ", ";
        }
        String first_img;
        if (!photo_paths.isEmpty()) {
            first_img = photo_paths.split("/")[0];
        } else {
            first_img = "";
        }
        System.out.println("1111111111111111111111111111111111111111");
        System.out.println(first_img);
        Post post = new Post(userId, title, body, price, date, transmission,
                fuel, color, doors, seats, mileage, kw, cylinder, number_of_cylinders,
                make, model, photo_paths);
        Map<String, Object> postValues = post.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        // for mailing admin about new offer
        //subject = title + ", " + price + " €";
        email_body = "<p><h3>" + title + "</h3></p>" + "<p><h4>" + price + "</h4></p>" + "<img src='" + first_img + "' style='width:400px; height:auto;' />" + "<p>" + body + "</p>";
        mailThread.start();

        if (!photos.isEmpty()) {
            StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
            for (final String p : photos) {
                final Uri photoUri = Uri.parse(p);
                StorageReference riversRef = storageRef.child("images/" + key + "/" + photoUri.getLastPathSegment());
                UploadTask uploadTask = riversRef.putFile(photoUri);

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();

                        // TODO: delete temp file from gallery
                        //File file = new File(p);
                        //boolean deleted = file.delete();
                        //System.out.println(deleted);
                    }
                });
            }
        }
        mDatabase.updateChildren(childUpdates);
    }
    // [END write_fan_out]


    Thread mailThread = new Thread(new Runnable() {
        @Override
        public void run() {
            SendGrid sendgrid = new SendGrid("a.zuev","123123ru??F");
            SendGrid.Email email = new SendGrid.Email();
            email.addTo(current_user.getEmail());
            System.out.println(current_user.getEmail());
            email.setFrom("info@carzando.com");
            email.setSubject("New offer");
            //email.setText(subject);
            email.setHtml(email_body);
            System.out.println(email_body);
            try {
                SendGrid.Response response = sendgrid.send(email);
            }
            catch (SendGridException e) {
                Log.e("sendError", "Error sending email");
            }
        }
    });

    public void datePicker(View view){
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "date");
    }

    private void setDate(final Calendar calendar) {
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
        ((TextView) findViewById(R.id.showDate)).setText(dateFormat.format(calendar.getTime()));

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar cal = new GregorianCalendar(year, month, day);
        setDate(cal);
    }

    public static class DatePickerFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener)
                            getActivity(), year, month, day);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void addImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(NewPostActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(NewPostActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectImage() {
        final CharSequence[] items = { "Remove current photo", "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(NewPostActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(NewPostActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();

                } else if (items[item].equals("Remove current photo")) {
                    userChoosenTask = "Remove current photo";

                    photos.remove(selImagePosition);
                    String[] stockArr = new String[photos.size()];
                    stockArr = photos.toArray(stockArr);
                    imageAdapter = new ImageAdapter(getBaseContext(), stockArr);
                    lvMain.setAdapter(imageAdapter);

                    ListViewUtility.setListViewHeightBasedOnChildren(lvMain);

                    selImagePosition = -1;
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        String imageFileName = System.currentTimeMillis() + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
        File file = new File(pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);}
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }

        String[] stockArr = new String[photos.size()];
        stockArr = photos.toArray(stockArr);
        imageAdapter = new ImageAdapter(this, stockArr);
        lvMain.setAdapter(imageAdapter);

        ListViewUtility.setListViewHeightBasedOnChildren(lvMain);
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    private void onCaptureImageResult(Intent data) {

        File imgFile = new File(pictureImagePath);
        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            Bitmap scaledBitmap = scaleDown(myBitmap, MAX_IMAGE_SIZE, true);
            Uri tempUri = getImageUri(getApplicationContext(), scaledBitmap);
            File finalFile = new File(getRealPathFromURI(tempUri));
            try {
                java.net.URI juri = new java.net.URI(finalFile.toURI().toString());
                if (selImagePosition == -1) {
                    photos.add(juri.toString());
                }
                else {
                    photos.remove(selImagePosition);
                    photos.add(selImagePosition, juri.toString());
                    selImagePosition = -1;
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(
                inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    //@SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImage = data.getData();
        String url;
        try {
            Bitmap scaledBitmap;
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inSampleSize = 1;
            Bitmap myBitmap = BitmapFactory.decodeStream(getBaseContext().getContentResolver().openInputStream(selectedImage), null, o);
            if (myBitmap.getWidth() > MAX_IMAGE_SIZE || myBitmap.getHeight() > MAX_IMAGE_SIZE) {
                scaledBitmap = scaleDown(myBitmap, MAX_IMAGE_SIZE, true);
                Uri tempUri = getImageUri(getApplicationContext(), scaledBitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                url = finalFile.toURI().toString();
                try {
                    java.net.URI juri = new java.net.URI(url);
                    if (selImagePosition == -1) {
                        photos.add(juri.toString());
                    } else {
                        photos.remove(selImagePosition);
                        photos.add(selImagePosition, juri.toString());
                        selImagePosition = -1;
                    }
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                if (selImagePosition == -1) {
                    photos.add(selectedImage.toString());
                }
                else {
                    photos.remove(selImagePosition);
                    photos.add(selImagePosition, selectedImage.toString());
                    selImagePosition = -1;
                }
            }
        } catch (FileNotFoundException e) {}
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setMessage("Auto is aangepast wilt u aanpassingen bewaren?")
            .setCancelable(false)
            .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    NewPostActivity.this.finish();
                }
            })
            .setNegativeButton("Geen", null)
            .show();
    }
}
