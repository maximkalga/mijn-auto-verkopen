package com.carzando.mijn_auto_verkopen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.carzando.mijn_auto_verkopen.models.Post;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


public class PostDetailActivity extends BaseActivity {

    private static final String TAG = "PostDetailActivity";

    public static final String EXTRA_POST_KEY = "post_key";

    private DatabaseReference mPostReference;
    private DatabaseReference mUserPostReference;
    private ValueEventListener mPostListener;
    private String mPostKey;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    private TextView mTitleView;
    private TextView mBodyView;
    private TextView mPriceView;
    private TextView mDateView;
    private TextView mTransmissionView;
    private TextView mColorView;
    private TextView mFuelView;
    private TextView mDoorsView;
    private TextView mSeatsView;
    private TextView mMileageView;
    private TextView mKwView;
    private TextView mCylinderView;
    private TextView mNumberOfCylindersView;
    private TextView mMakeView;
    private TextView mModelView;

    private String [] photosToDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Initialize Views
        mTitleView = (TextView) findViewById(R.id.post_title);
        mBodyView = (TextView) findViewById(R.id.post_body);
        mPriceView = (TextView) findViewById(R.id.post_price);
        mDateView = (TextView) findViewById(R.id.post_date);
        mTransmissionView = (TextView) findViewById(R.id.post_transmission);
        mColorView = (TextView) findViewById(R.id.post_color);
        mFuelView = (TextView) findViewById(R.id.post_fuel);
        mDoorsView = (TextView) findViewById(R.id.post_doors);
        mSeatsView = (TextView) findViewById(R.id.post_seats);
        mMileageView = (TextView) findViewById(R.id.post_mileage);
        mKwView = (TextView) findViewById(R.id.post_kw);
        mCylinderView = (TextView) findViewById(R.id.post_cylinder);
        mNumberOfCylindersView = (TextView) findViewById(R.id.post_number_of_cylinders);
        mMakeView = (TextView) findViewById(R.id.post_make);
        mModelView = (TextView) findViewById(R.id.post_model);

        // Get post key from intent
        mPostKey = getIntent().getStringExtra(EXTRA_POST_KEY);
        if (mPostKey == null) {
            throw new IllegalArgumentException("Must pass EXTRA_POST_KEY");
        }

        // Initialize Database
        final String userId = getUid();
        mPostReference = FirebaseDatabase.getInstance().getReference()
                .child("posts").child(mPostKey);
        mUserPostReference = FirebaseDatabase.getInstance().getReference()
                .child("user-posts").child(userId).child(mPostKey);
        final StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");

        ValueEventListener photoPathsListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Post post = dataSnapshot.getValue(Post.class);
                    photosToDelete = post.photo_paths.split(", ");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                Toast.makeText(PostDetailActivity.this, "Failed to load post.",
                        Toast.LENGTH_SHORT).show();
            }
        };
        mPostReference.addValueEventListener(photoPathsListener);

        findViewById(R.id.btn_remove_offer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(PostDetailActivity.this)
                    .setMessage("Are you sure you want to remove offer?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(PostDetailActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            mUserPostReference.removeValue();
                            mPostReference.removeValue();

                            // Delete the photos
                            for (String p : photosToDelete) {
                                StorageReference desertRef = storageRef.child(p);
                                desertRef.delete();
                            }
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
            }

        });

        findViewById(R.id.btn_edit_offer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PostDetailActivity.this, EditPostActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(PostDetailActivity.EXTRA_POST_KEY, mPostKey);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        // Add value event listener to the post
        // [START post_value_event_listener]
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                try {
                    Post post = dataSnapshot.getValue(Post.class);

                    TableLayout table = (TableLayout) findViewById(R.id.offerTableLayout);

                    // [START_EXCLUDE]
                    mTitleView.setText(post.title);
                    mBodyView.setText(post.body);
                    mPriceView.setText("€ " + post.price);
                    mDateView.setText(post.date);
                    mMakeView.setText(post.make);
                    mModelView.setText(post.model);
                    mMileageView.setText(post.mileage);

                    if (post.transmission.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowTransmission));
                    } else {
                        mTransmissionView.setText(post.transmission);
                    }
                    if (post.color.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowColor));
                    } else {
                        mColorView.setText(post.color);
                    }
                    if (post.fuel.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowFuel));
                    } else {
                        mFuelView.setText(post.fuel);
                    }
                    if (post.doors.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowDoors));
                    } else {
                        mDoorsView.setText(post.doors);
                    }
                    if (post.seats.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowSeats));
                    } else {
                        mSeatsView.setText(post.seats);
                    }
                    if (post.kw.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowKw));
                    } else {
                        mKwView.setText(post.kw);
                    }
                    if (post.cylinder.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowCylinder));
                    } else {
                        mCylinderView.setText(post.cylinder);
                    }
                    if (post.number_of_cylinders.isEmpty()) {
                        table.removeView((TableRow) findViewById(R.id.tableRowNumberOfCylinders));
                    } else {
                        mNumberOfCylindersView.setText(post.number_of_cylinders);
                    }
                    if (post.photo_paths.length() > 0) {
                        String[] photos = post.photo_paths.split(", ");
                        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPageAndroid);
                        AndroidImageAdapter adapterView = new AndroidImageAdapter(getBaseContext(), photos);
                        mViewPager.setAdapter(adapterView);
                    } else {
                        table.removeView((TableRow) findViewById(R.id.tableRowPhotos));
                    }
                } catch (NullPointerException e) {};
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // [START_EXCLUDE]
                Toast.makeText(PostDetailActivity.this, "Failed to load post.",
                        Toast.LENGTH_SHORT).show();
                // [END_EXCLUDE]
            }
        };
        mPostReference.addValueEventListener(postListener);
        // [END post_value_event_listener]

        // Keep copy of post listener so we can remove it when app stops
        mPostListener = postListener;
    }

    @Override
    public void onStop() {
        super.onStop();

        // Remove post value event listener
        if (mPostListener != null) {
            mPostReference.removeEventListener(mPostListener);
        }
    }
}
