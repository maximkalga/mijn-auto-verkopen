package com.carzando.mijn_auto_verkopen;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ImageAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    public ImageAdapter(Context context, String[] values) {
        super(context, R.layout.rowlayout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
        final String photo_path = values[position];
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        textView.setText(photo_path);
        if (photo_path.contains("/images/-")) {
            StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
            storageRef.child(photo_path).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    new DownloadImageTask((ImageView) rowView.findViewById(R.id.icon)).execute(uri.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // TODO: добавить задержку и повторить загрузку
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
                            storageRef.child(photo_path).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    new DownloadImageTask((ImageView) rowView.findViewById(R.id.icon)).execute(uri.toString());
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // TODO: добавить задержку и повторить загрузку
                                }
                            });
                        }
                    }, 3000);
                }
            });
        } else {
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            imageView.setImageURI(Uri.parse(photo_path));
        }

        return rowView;
    }
}
