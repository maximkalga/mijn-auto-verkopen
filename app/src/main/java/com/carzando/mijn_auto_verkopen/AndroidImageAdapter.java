package com.carzando.mijn_auto_verkopen;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class AndroidImageAdapter extends PagerAdapter {
    Context mContext;
    private String[] values;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    AndroidImageAdapter(Context context, String[] values) {
        this.mContext = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int i) {
        final ImageView mImageView = new ImageView(mContext);
        final String photo_path = values[i];
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
        storageRef.child(photo_path).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                try {
                    new DownloadImageTask((mImageView)).execute(uri.toString());
                } catch (Exception e) {
                    System.out.println(e);
                }
                ((ViewPager) container).addView(mImageView, 0);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // TODO: добавить задержку и повторить загрузку
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        StorageReference storageRef = storage.getReferenceFromUrl("gs://mijn-auto-verkopen.appspot.com");
                        storageRef.child(photo_path).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                try {
                                    new DownloadImageTask((mImageView)).execute(uri.toString());
                                } catch (Exception e) {}
                                ((ViewPager) container).addView(mImageView, 0);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // TODO: добавить задержку и повторить загрузку
                            }
                        });
                    }
                }, 1500);
            }
        });

        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }

}